#! /usr/bin/env python3
from Intcode import IntCode
import itertools

class Day7:
    def __init__(self):
        self.intCode = IntCode('input')

    def findMaxCodeOnePass(self, numInputs):
        #all inputs are unique, if program has error, will discard and try new
        # combo

        outDict = dict()
        out = None
        r = numInputs

        for a, b, c, d, e in\
            itertools.product(range(r), range(r), range(r), range(r), range(r)):
            phaseArr = [a, b, c, d, e]
            if len(phaseArr) == len(set(phaseArr)):
                inputs = phaseArr.copy()
                arr = [phaseArr.pop(0), 0]
                c = 0
                while c < r:
                    try:
                        out = self.intCode.runProgWithInputsNoState(arr)
                        print(out)
                        arr = [phaseArr.pop(0), out]
                    except:
                        break
                if out:
                    outDict[out] = inputs
                c += 1

        print({k: outDict[k] for k in sorted(outDict)})

    def findMaxCodeFeedback(self, lowerB, upperB):
        #all inputs are unique, if program has error, will discard and try new
        # combo

        outDict = dict()
        out = None
        r = upperB - lowerB + 1
        try:
            for a, b, c, d, e in\
                itertools.product(range(r), range(r), range(r), range(r), range(r)):
                phaseArr = [a, b, c, d, e]
                phaseArr = [x + r for x in phaseArr]
                if len(phaseArr) == len(set(phaseArr)):
                    progArr = [[True, IntCode('input')] for x in range(r)]
                    inputs = phaseArr.copy()
                    arr = [phaseArr.pop(0), 0]
                    c = 0
                    cont = True
                    while progArr[c % r][0]:
                    #    try:
                        out, progArr[c % r][0] = progArr[c % r][1].runProgWithInputsWithState(arr)
                        if c < r - 1:
                            arr = [phaseArr.pop(0), out]
                        else:
                            arr = [out]
#                        except:
#                            break
                        c += 1

                    if out and not progArr[r - 1][0] :
                        outDict[out] = inputs

            print({k: outDict[k] for k in sorted(outDict)})
        except KeyboardInterrupt:
            sys.exit()

    def test(self):
        self.intCode = IntCode('testinput')
        outDict = dict()
        out = None
        phaseArr = [4, 3, 2, 1, 0]
        r = len(phaseArr)
        if len(phaseArr) == len(set(phaseArr)):
            inputs = phaseArr.copy()
            arr = [phaseArr.pop(0), 0]
            c = 0
            while c < r:
                out = self.intCode.runProgWithInputs(arr)
                print(phaseArr)
                print(out)
                if phaseArr:
                    arr = [phaseArr.pop(0), out]
                c += 1
            if out:
                outDict[out] = inputs

        print({k: outDict[k] for k in sorted(outDict)})


Day7().findMaxCodeFeedback(5, 9)
