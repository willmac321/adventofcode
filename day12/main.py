#! /usr/bin/env python3
import sys, os
import re
from timer import timer

class Position:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    @property
    def tuple(self):
        return (self.x, self.y, self.z)

class Velocity:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    @property
    def tuple(self):
        return (self.x, self.y, self.z)

class Moon:
    def __init__(self, p, v = (0, 0, 0)):
        self.position = Position(p[0],p[1],p[2])
        self.velocity = Velocity(v[0],v[1],v[2])

    def toString(self):
        return 'pos=<x= {}, y= {}, z= {}>, vel=<x= {}, y= {}, z= {}>'\
                .format(self.position.x, self.position.y, self.position.z, self.velocity.x, self.velocity.y, self.velocity.z)
    def compareTo(self, m):
        return self.position.tuple == m.position.tuple and self.velocity.tuple == m.velocity.tuple

class Main:
    def __init__(self, file):
        with open(file) as f:
            self.items = f.read()
        self.items = self.items.splitlines()
        self.moons = []
        self.old_moons = []
        for item in self.items:
            temp = re.compile(r'-?\d+')
            temp = temp.findall(item)
            self.moons.append(Moon((int(temp[0]), int(temp[1]), int(temp[2]))))
            self.old_moons.append(Moon((int(temp[0]), int(temp[1]), int(temp[2]))))

    @timer
    def timeStep(self):
        i = 0
        while i < 1000:
            self.updateVelocities()
            self.updatePositions()
            i += 1
        self.printMoons()
        self.findTotalEnergy()

    def updatePositions(self):
        for k, moon in enumerate(self.moons):
            self.moons[k].position.x = moon.position.x + moon.velocity.x
            self.moons[k].position.y = moon.position.y + moon.velocity.y
            self.moons[k].position.z = moon.position.z + moon.velocity.z

    def findTotalEnergy(self):
        energy = 0
        for moon in self.moons:
            p = moon.position.tuple
            pot = 0
            for l in p:
                pot += abs(l)
            p = moon.velocity.tuple
            vel = 0
            for l in p:
                vel += abs(l)

            energy += pot * vel

        print(energy)
        
    def updateVelocities(self):
        new_moon_vel = []
        #if x < y =  y -- and x ++
        #check gravity
        for k1 in range(3):
            moon1 = self.moons[k1]
            for k2 in range(k1 + 1, 4):
                moon2 = self.moons[k2]
                #calc new velocities
                vel = moon1.velocity.tuple
                vel2 = moon2.velocity.tuple
                for i, pos in enumerate(moon1.position.tuple):
                    # if not same position
                    if pos < moon2.position.tuple[i]:
                        q = k1
                        w = k2
                        if i == 0:
                            self.moons[q].velocity.x += 1
                            self.moons[w].velocity.x -= 1
                        elif i == 1:
                            self.moons[q].velocity.y += 1
                            self.moons[w].velocity.y -= 1
                        elif i == 2:
                            self.moons[q].velocity.z += 1
                            self.moons[w].velocity.z -= 1
                    elif pos > moon2.position.tuple[i]:
                        q = k2
                        w = k1
                        if i == 0:
                            self.moons[q].velocity.x += 1
                            self.moons[w].velocity.x -= 1
                        elif i == 1:
                            self.moons[q].velocity.y += 1
                            self.moons[w].velocity.y -= 1
                        elif i == 2:
                            self.moons[q].velocity.z += 1
                            self.moons[w].velocity.z -= 1

    def matches(self, v, v_old, p, p_old, steps): 
        for i in range(4):
            if p[i] != p_old[i] or \
               v[i] != v_old[i]:
                return False
        return True

    def printMoons(self):
        for moon in self.moons:
            print(moon.toString())
    
    @timer
    def findFirstRepeats(self):
        cont = True
        counter = 0
        # check by x, y, z not moon
        steps = [0, 0, 0]
        cont_match = [True, True, True]
        v_old = [[moon.velocity.tuple[x] for moon in self.old_moons] for x in range(3)]
        p_old = [[moon.position.tuple[x] for moon in self.old_moons] for x in range(3)]

        i = 0
        while i < 3:
            self.updateVelocities()
            self.updatePositions()
            counter += 1

            v = [[moon.velocity.tuple[x] for moon in self.moons] for x in range(3)]
            p = [[moon.position.tuple[x] for moon in self.moons] for x in range(3)]
            for j in range(3):
                if self.matches(v[j], v_old[j], p[j], p_old[j], steps) and cont_match[j]:
                    i += 1
                    steps[j] = counter
                    cont_match[j] = False
        print(steps)

        print(self.calculate_lcm(steps))

    def get_prime_factors(self, n):
        i = 2
        factors = []
        while i * i <= n:
            if n % i:
                i += 1
            else:
                n //= i
                factors.append(i)
        if n > 1:
            factors.append(n)
        return factors

    def calculate_lcm(self, steps):
        primes_per_dimension = [self.get_prime_factors(step) for step in steps]
        all_primes = set([prime for primes in primes_per_dimension for prime in primes])
        
        lcm = 1    
        for prime in all_primes:
            amount = max(primes_per_dimension[dim].count(prime) for dim in range(3))
            lcm *= prime**amount
        return lcm

        


if len(sys.argv)>1:
    f = sys.argv[1]
    if os.path.exists(f):
#        Main(f).timeStep()
        Main(f).findFirstRepeats()


