#! /usr/bin/env python3

import itertools

class day2:
    def __init__(self):
        with open('day2input', 'r') as f:
            self.items = f.read().split(",")
        self.items = [int(item) for item in self.items]
        self.opCode = self.convertToOpCode()

    def convertToOpCode(self):
        # create a more easily human readable format
        tempList = list()
        arr = []
        count = 0
        maxC = 0
        for key, item in enumerate(self.items):
            if count == 0:
                if item == 1 or item == 2 or item != 99:
                    maxC = 3
                elif item == 99:
                    maxC = 0
            if count < maxC:
                arr.append(item)
                count += 1
            else:
                arr.append(item)
                tempList.append(arr)
                count = 0
                arr = []
        return tempList           

    def runProg(self):
        for i in range(0, len(self.items), 4):
            if self.items[i] == 1:
                temp = self.items[self.items[i + 1]] + self.items[self.items[i + 2]]
#                self.printOp(i, temp)
                self.items[self.items[i + 3]] = temp
            elif self.items[i] == 2:
                temp = self.items[self.items[i + 1]] * self.items[self.items[i + 2]]
#                self.printOp(i, temp)
                self.items[self.items[i + 3]] = temp
            elif self.items[i] == 99:
#                self.printOp(i, 0)
                break
        self.opCode = self.convertToOpCode()

    def printOp(self, i, temp):
        if(self.items[i] != 99):
            print('{}, i1: {}, i2: {}, out0: {}, outN: {}'
                    .format(
                        self.items[i:i+4], 
                        self.items[self.items[i+1]], 
                        self.items[self.items[i+2]], 
                        self.items[self.items[i+3]],
                        temp)
                    )
        else:
            print(self.items[i])

    def updateOpCode(self, index, value):
        self.items[index] = value
        self.opCode = self.convertToOpCode()

    def updateVals(self, i1, i2):
        self.items[1] = i1
        self.items[2] = i2

    def solveForVal(self, v):
        oldItems = self.items.copy()
        for i, j in itertools.product(range(100), range(100)):
            self.items = oldItems.copy()
            self.updateVals(i, j)
            print(self.items[0:4])
            self.runProg()
            if self.items[0] == v:
                break
        print('noun: {}, verb: {}, out: {}'
                .format(self.items[1],
                    self.items[2], 
                    100 * self.items[1]+self.items[2])
                )
         
"""
        for key, item in enumerate(self.opCode):
            for k, i in enumerate(item):
                if count == index:
                    self.opCode[key][k] = value
                    break
                else:
                    count += 1         
"""                    

prob2 = day2()
print(prob2.opCode)
print()
prob2.solveForVal(19690720)
print(prob2.opCode)
