#! /usr/bin/env python3

class paintPad:
    def __init__(self):
        self.move_arr = []
        self.move_set = set()
        self.painted_tile = dict()
        self.move_arr.append((0,0))
        #0 is N going clockwise
        self.orientation = 0

    def append(self, item):
        #0 is Black
        # row, column ordering
        self.move_arr.append(item)
        self.move_set.add(item)

    def getPaintColor(self, loc):
        if loc in self.painted_tile.keys():
            return self.painted_tile[loc]
        else:
            self.painted_tile[loc] = 0
            return 0

    def paintAndMove(self, paint, move):
        lastLocation = self.move_arr[-1]
        self.painted_tile[lastLocation] = paint

        color = '.'
        
        if paint == 1:
            color = '#' 
        
        new_loc, m = self.move(move, lastLocation)
        self.append(new_loc)

        return new_loc, color, m

    def move(self, move, location):
        # 0 is left 1 is right
        if move == 0:
            self.orientation = (4 + self.orientation - 1) % 4
        elif move == 1:
            self.orientation = (4 + self.orientation + 1) % 4
        else:
            raise ValueError('what is happening')

        o = self.orientation
        m = ''
        if o == 0:
            location = (location[0] - 1, location[1])
            m = 'go up'
        elif o == 1:
            location = (location[0], location[1] + 1)
            m = 'go right'
        elif o == 2:
            location = (location[0] + 1, location[1])
            m = 'go down'
        elif o == 3:
            location = (location[0], location[1] - 1)
            m = 'go left'
        else:
            raise ValueError('what is happening')

        return location, m


