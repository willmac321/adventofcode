#! /usr/bin/env python3

class Day6:
    def __init__(self):
        with open('day6input', 'r') as f:
            self.items = f.read().split()
        self.graph = dict()
        self.constructGraph(self.graph)
        self.costs = {x: float("inf") for x in list(self.graph)}

    def constructGraph(self, graph):
        for item in self.items:
            source, orbiter = item.split(")")
            if source in graph:
                graph[source].append(orbiter)
            else:
                graph[source] = [orbiter]
            if orbiter not in graph:
                graph[orbiter] = []

        for s in list(graph):
            for p in graph[s]:
                if s not in graph[p]:
                    graph[p].append(s)

        # 0 is unvisited 1 is visited
        for s in list(graph):
            graph[s].append(0)

    def depthFirst(self, graph, origin, cost):
        if origin in graph and graph[origin][-1] == 0:
            graph[origin][-1] = 1
            if self.costs[origin] > cost:
                self.costs[origin] = cost
            cost += 1
            adj = graph[origin]
            for a in adj:
                if a != 1 or a != 0 and adj[-1] == 0:
                    graph = self.depthFirst(graph, a, cost)
            return graph
        else:
            return graph

    def totalEdges(self):
        total = 0
        for c in self.costs:
            total += self.costs[c]
        return total

    def findSanta(self, graph, s, d):
        for g in graph:
            if s in graph[g]:
                source = g
            if d in graph[g]:
                dest = g
        print('{} orbits {} and {} orbits {}'.format(s, source, d, dest))
        self.graph = self.depthFirst(self.graph, source, 0)
        return self.costs[dest]


day6 = Day6()
print(day6.findSanta(day6.graph, 'YOU', 'SAN'))
