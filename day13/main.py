#! /usr/bin/env python3
from IntCodeOptions import IntcodeOptions
import painting
import itertools
import curses
import logging
import sys, os
from curses.textpad import Textbox, rectangle
from timer import timer

class stdOut:
    def __init__(self):
        text = ""
        if os.path.exists('out.log'):
            os.remove('out.log')
        logging.basicConfig(level=logging.INFO, filename='out.log')
    def write(self, txt):
        logging.info(txt)

class main:
    def __init__(self):
        self.icHandler = IntcodeOptions('input')
        self.log = stdOut()
        self.score = 0
        self.menu = [
        'Show Program',
        'Play Game',
        'Play Game Computer',
        'Reset',
        'Exit',
        ]
        self.old_arr = []
        self.count = 0
        self.outputArr = []
        self.maxL = 0
        curses.wrapper(self.startCurses)
#        print(self.icHandler.intCode.items)
#        print(self.icHandler.arr2D)

    def startCurses(self, stdscr):
        curses.noecho()
        # Clear and refresh the screen for a blank canvas
        stdscr.clear()
        stdscr.refresh()

        # Start colors in curses
        curses.start_color()
        # these are for UI
        curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
        curses.init_pair(4, curses.COLOR_BLACK, curses.COLOR_CYAN)

        curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
        curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)
        curses.init_pair(5, curses.COLOR_WHITE, curses.COLOR_BLACK)
        curses.init_pair(6, curses.COLOR_BLACK, curses.COLOR_GREEN)
        curses.init_pair(7, curses.COLOR_CYAN, curses.COLOR_WHITE)
        curses.init_pair(8, curses.COLOR_RED, curses.COLOR_BLUE)

        self.run(stdscr)

    def updateMenu(self, cursor_y, menuscr, exec, runOpt):
        # Declaration of strings
        statusbarstr = "Press 'q' to exit | {} | {}".format(exec, runOpt)

        # Render status bar
        m_h, m_w = menuscr.getmaxyx()
        menuscr.attron(curses.color_pair(3))
        menuscr.addstr(m_h-2, 1, statusbarstr)
        menuscr.addstr(m_h-2, len(statusbarstr) + 1, " " * (m_w - len(statusbarstr) - 2))
        menuscr.attroff(curses.color_pair(3))

        # Render menu text
        menuscr.attron(curses.color_pair(1))
        i = 0
        for m in self.menu:
            if(cursor_y == i):
                menuscr.attroff(curses.color_pair(1))
                menuscr.attron(curses.color_pair(4))
            menuscr.addstr(2 + i, 3, m)
            menuscr.addstr(2 + i, len(m) + 3, " " * (m_w - len(m) - 7))
            if(cursor_y == i):
                menuscr.attroff(curses.color_pair(4))
                menuscr.attron(curses.color_pair(1))
            i += 1
        menuscr.attroff(curses.color_pair(1))

        return cursor_y

    def drawLog(self, scr):
        m_h, m_w = scr.getmaxyx()

        if len(self.outputArr) > 1 and len(self.outputArr) > m_h * (m_w // self.maxL):
            self.outputArr.pop(0)

        row = m_w // self.maxL
        extra_w = (m_w - row * self.maxL) // row

        # Render menu text
        scr.attron(curses.color_pair(1))
        i = 0
        j = 0
        l = 0

#        self.log.write('new output print****************')

        for m in self.outputArr:
            if i >= m_h:
                j += (self.maxL + extra_w)
                i = 0
            if l == len(self.outputArr) - 1:
                scr.attron(curses.color_pair(4))
#            self.log.write('   m_h:{} i: {} j: {} m: {} row: {} extra_w: {} maxL: {}'.format(m_h, i, j, m, row, extra_w, self.maxL))
            scr.addstr(i, j, " " * (self.maxL + extra_w))
            scr.addstr(i, j, "{:<{}}".format(m, self.maxL + extra_w))
            if l == len(self.outputArr) - 1:
                scr.attroff(curses.color_pair(4))
            i += 1
            l += 1
        scr.attroff(curses.color_pair(1))
        scr.refresh()
#        except:
#            scr.attron(curses.color_pair(2))
#            scr.addstr(0,0, '{} {} {}'.format(row, self.maxL, j))
#            scr.attron(curses.color_pair(2))


    def pushMessage(self, messagescr, m):
        y, x = messagescr.getmaxyx()
        oldMaxL = self.maxL
        self.maxL = 0

        self.outputArr.append(m)

        for i in self.outputArr:
            if len(i) + 2 > self.maxL:
                self.maxL = len(i) + 2

        if oldMaxL != 0 and x // self.maxL != x // oldMaxL:
            messagescr.clear()

        oldMaxL = self.maxL

        row = x // (self.maxL) 
        extra_w = (x - row * self.maxL) // row

        if len(self.outputArr) > (y) * row - 1:
            messagescr.clear()
            messagescr.refresh()

        while len(self.outputArr) > (y) * row - 1:
            self.outputArr.pop(0) 

    def drawNewProg(self, printscr, h, w, arr):
        self.count = 0
        for y in range(len(arr)):
            for x in range(len(arr[y])):
                rx = self.icHandler.maxItemWidth
                a = arr[y, x]
                printscr.addstr(y , x * rx + 1, '{:^{}}'.format(a, rx))
                self.count += 1

            printscr.refresh()
        self.old_arr = arr.copy()

    def drawPrint(self, printscr, arr, j):
        new_count = 0
        for y in range(len(arr)):
            for x in range(len(arr[y])):
                new_count += 1

        for y in range(len(arr)):
            for x in range(len(arr[y])):
                rx = self.icHandler.maxItemWidth
                a = arr[y][x]
                if self.count != new_count:
                    printscr.addstr(y , x * rx + 1, '{:^{}}'.format(a, rx))
                elif self.old_arr[y][x] != a:
                    printscr.addstr(y , x * rx + 1, '{:^{}}'.format(a, rx))
                    printscr.chgat(y, x * rx + 1, rx, curses.color_pair(6))
                else:
                    printscr.addstr(y , x * rx + 1, '{:^{}}'.format(a, rx))
                printscr.refresh()
                curses.napms(0)

        self.count = new_count
        self.old_arr = arr.copy()

    def gatherInput(self, messagescr, val, cont):
        messagescr.clear()
        messagescr.attron(curses.color_pair(3))
        messagescr.addstr(0, 1, val)
        self.log.write('input val {}'.format(val))
        inputscr = messagescr.derwin(0, 10, 0, len(val) + 3)
        inputscr.attron(curses.color_pair(3))
        messagescr.refresh()
        box = Textbox(inputscr)
        box.edit()
        m = box.gather()
        if 'q' in m:
            cont = False
            m = 'Execution Halted'
        messagescr.clear()
        messagescr.refresh()
        return m, cont

    def runUserModeSingle(self, printscr, messagescr, textscr, h, w, arr):
        cont = True
        j = 0
        inputArray = []
        printscr.attron(curses.color_pair(1))
        while cont:
            self.drawPrint(printscr, arr, j)
            val, cont, arr = self.icHandler.runSingle(True, inputArray)
            if arr == 'input':
                inputArray = []
                val, cont = self.gatherInput(messagescr, val, cont)
                if cont:
                    messagescr.attroff(curses.color_pair(3))
                    arr = None
                    inputArray = [int(m)]
                    messagescr.refresh()
                self.pushMessage(textscr, val + str(m))
            elif arr == 'output':
                messagescr.clear()
                messagescr.attron(curses.color_pair(3))
                messagescr.addstr(0, 1, str(val))
                messagescr.refresh()
                messagescr.attroff(curses.color_pair(3))
                self.pushMessage(textscr, val)
                arr = None
                inputArray = []
            elif arr == 'end':
                j = 0
                messagescr.clear()
                messagescr.attron(curses.color_pair(3))
                messagescr.addstr(0, 1, str(val))
                messagescr.refresh()
                messagescr.attroff(curses.color_pair(3))
                self.pushMessage(textscr, val)
                inputArray = []
            elif arr != []:
                j = arr
                messagescr.clear()
                messagescr.attron(curses.color_pair(3))
                messagescr.addstr(0, 1 , str(val))
                messagescr.refresh()
                messagescr.attroff(curses.color_pair(3))
                inputArray = []
            curses.napms(0)
            self.icHandler.setSnapshot(w, h, j)
            arr = self.icHandler.arr2D
            self.drawPrint(printscr, arr, j)
            self.drawLog(textscr)
        printscr.attroff(curses.color_pair(1))

    def paintGrid(self, printscr,textscr,messagescr, h, w, i = 0, loop = True):


        painter = painting.paintPad()
        out = []
        mess = [] 

        out, mess, cont, i = self.icHandler.getTiles(i, loop)

        self.log.write(mess)
        o = None
        paddle = None


        out, score = painter.drawMap(out)
        if mess == 'end':
            return self.score, False, 0, None, None

        for i in range(len(out), -1, -3):
            if out[i - 1] == 'o':
                self.log.write(out[i - 3:i])
                o = out[i - 3:i]
                break
        for i in range(len(out), -1, -3):
            if out[i - 1] == '-':
                self.log.write(out[i - 3:i])
                paddle = out[i - 3:i]
                break

        if self.score < int(score):
            self.score = int(score)
        self.log.write(cont)
        self.log.write(out) 
        starty, startx = 0, 0

        count = painter.painted_tile
        if self.score:
            self.pushMessage(textscr, 'score: {}'.format(self.score) )

#        self.pushMessage(textscr, 'painted {}'.format(count) )
        self.drawLog(textscr)
        self.drawSquares(printscr, startx, starty, out)
        printscr.refresh()
        return self.score, cont, i, o, paddle

    def playGameComputer(self, printscr, textscr, messagescr, h, w):
        self.outputArr = []
        self.icHandler.reset()
        self.pushMessage(textscr,'start')
        self.score = 0

        self.icHandler.set(0, 2)

        cont = True
        k = None
        i = 0
        paddle = None

        render = True

        while cont:
            score, cont, i, o, p = self.paintGrid(printscr, textscr, messagescr, h, w, i, render)
            if not paddle:
                paddle = p
            elif p and (p[0] != paddle[0] or p[1] != paddle[1] or paddle[2] != p[2]):
                paddle = p

            render = False
            if score > 0 and not cont and i == 0:
                cont = False
            else:
                cont = True
                inp = [0]

                if o and paddle:
                    if o[0] > paddle[0]:
                        inp = [1]
                    elif o[0] < paddle[0]:
                        inp = [-1]
                self.log.write(o)
                self.log.write(paddle)

            self.icHandler.updateInArray(inp)

            printscr.addstr(10, w - 20, str(score))


    def playGame(self, printscr, textscr, messagescr, h, w, stdscr):
        self.score = 0
        self.outputArr = []
        self.icHandler.reset()
        printscr.clear()
        printscr.refresh()
        self.pushMessage(textscr,'start')

        self.icHandler.set(0, 2)

        cont = True
        inp = [0]
        k = None
        printscr.nodelay(True)
        i = 0
        render = True

        while cont:
            score, cont, i, o = self.paintGrid(printscr, textscr, messagescr, h, w, i, render)
            cont = True
            if not render: 
                curses.napms(100)
                k = printscr.getch()

            render = False

            if k == ord('j'):
                inp = [-1]
            elif k == ord('l'):
                inp = [1]
            elif k == ord('q'):
                cont = False
            else:
                inp = [0]

            

            self.icHandler.updateInArray(inp)

            printscr.addstr(10, w - 20, str(score))
            curses.napms(1)

        printscr.nodelay(False)


    def drawSquares(self, printscr, sx, sy, out):
        r, c = printscr.getmaxyx()

        for i in range(0, len(out), 3):
            if out[i] != -1:
                row, col = out[i + 1] + sy, out[i] + sx
                printscr.addstr(row, col, out[i + 2])
                printscr.chgat(row, col, 1, curses.color_pair(1))
                printscr.refresh()

                curses.napms(0)

    def runProg(self, i, printscr, messagescr, textscr, stdscr):
        k = None
        h, w = printscr.getmaxyx()
        mh, mw = messagescr.getmaxyx()
        self.icHandler.setSnapshot(w, h, 0)
        arr = self.icHandler.arr2D
        ih, ix = self.icHandler.height, self.icHandler.width
        self.pushMessage(textscr,'start')
        if i == 0:
            printscr.clear()
            printscr.attron(curses.color_pair(1))
            self.drawNewProg(printscr, h, w, arr)
            printscr.attroff(curses.color_pair(1))
            printscr.refresh()
            messagescr.refresh()
        elif i == 1:
            printscr.clear()
            self.playGame(printscr,textscr,messagescr, h, w, stdscr)
        elif i == 2:
            printscr.clear()
            self.playGameComputer(printscr,textscr,messagescr, h, w)
        elif i == 4:
            printscr.clear()
            k = ord('q')
        elif i == 3:
            self.icHandler.reset()
        return k

    def run(self, stdscr):
        k = 0
        exec = 'IDLE'
        runOpt = ''
        cursor_y = 0

        height, width = stdscr.getmaxyx()

        # Create menu and output windows
        outscr = curses.newwin(int(height/8 * 3), width, 0, 0)
        outscr.box()
        menuscr = curses.newwin(int(height/4), width, int(height/4 * 3 ), 0)
        messagescr = curses.newwin(int(5 * height/8 - height/4 + 1), width, int(height/8 * 3), 0)
        messagescr.box()
        inputscr = curses.newwin(1 , width - 2, int(height/4 * 3) - 2, 1) 
        textoutscr = curses.newwin(int(5 * height/8 - height/4 + 1) - 4, width - 4, int(height/8 * 3) + 1, 2)
        ho, wo = outscr.getmaxyx()
        printscr = curses.newwin(ho - 2, wo - 2, 1 ,1)

        stdscr.clear()
        stdscr.refresh()
        outscr.refresh()
        messagescr.refresh()
        textoutscr.refresh()
        inputscr.refresh()

        # Loop where k is the last character pressed
        while (k != ord('q')):
            # Initialization
            curses.curs_set(0)
            menuscr.clear()


            # key moves
            if k == curses.KEY_DOWN or k == ord('j'):
                cursor_y = cursor_y + 1
            elif k == curses.KEY_UP or k == ord('k'):
                cursor_y = cursor_y - 1
            elif k == curses.KEY_ENTER or k == 10 or k == 13:
                exec = 'RUNNING'
                cursor_y = self.updateMenu(cursor_y, menuscr, exec, runOpt)
                menuscr.box()
                menuscr.refresh()
                k = self.runProg(cursor_y, printscr, inputscr, textoutscr, stdscr)
                exec = 'IDLE'

            cursor_y = max(0, cursor_y)
            cursor_y = min(len(self.menu) - 1, cursor_y)

            cursor_y = self.updateMenu(cursor_y, menuscr, exec, runOpt)

            # draw some borders
            menuscr.box()

            # refresh screen
            menuscr.refresh()
            printscr.refresh()

            # Wait for next input
            if k != ord('q'):
                k = stdscr.getch()

main()
