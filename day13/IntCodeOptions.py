#! /usr/bin/env python3
from Intcode import IntCode
import itertools
import curses
import numpy as np
import time

class IntcodeOptions:
    def __init__(self, file):
        self.intCode = IntCode(file)
        self.height = 0
        self.width = 0
        self.arr2D = []
        self.maxItemWidth = 1
        self.current_row = 0
        self.oldIndex = 0

    def add(self, i, v):
        self.intCode.items[i] += v

    def set(self, i, v):
        self.intCode.items[i] = v

    def reset(self):
        self.intCode.reset()

    def addToInputs(self, item):
        self.intCode.addToInput(item)

    def runSingle(self, userMode, inArr = []):
        # returns  value, continue, message
        return self.intCode.runProgWithCurses(inArr, userMode)

    def updateInArray(self, inArr):
        self.intCode.inputArr = inArr

    def getTiles(self, i, loop = True):
        cont = True
        vals = []
        mess = []
        j = 0

        while cont:
            v, cont, m = self.intCode.runProgWithCurses()
            if m == 'output':
                vals.append(v)
                mess.append(m)
                j += 1
                if j % 3 == 0 and not loop:
                    return vals, m, cont, i
            if m =='input':
                return vals, m, cont, i
#                if v == 4 and j % 3 == 0 and j > i:
#                    time.sleep(1)
        i = j
        return vals, m, cont, i



    def getItems(self):
        return self.intCode.items

    def setSnapshot(self, x, y, startIndex = 0):
        #find max item width and use that
        self.maxItemWidth = len(str(self.intCode.items[0])) + 2
        for i in self.intCode.items:
            if len(str(i)) + 2 > self.maxItemWidth:
                self.maxItemWidth = len(str(i)) + 2

        self.width = x // self.maxItemWidth
        rows = y 
        if rows > len(self.intCode.items) // self.width:
            startIndex = 0
        if divmod(startIndex, self.width)[0] == self.current_row:
            startIndex = self.oldIndex
        else:
            self.current_row = startIndex // self.width
            self.oldIndex = startIndex
        if rows > len(self.intCode.items[startIndex:]) // self.width:
            self.height = len(self.intCode.items[startIndex:]) // self.width + 1
        else:
            self.height = rows
        self.arr2D = np.zeros([self.height, self.width], dtype=np.int_)
        i = startIndex
        for row in range(self.height):
            for col in range(self.width):
                if i < len(self.intCode.items[startIndex:]):
                    self.arr2D[row, col] = self.intCode.items[i]
                    i += 1
        

