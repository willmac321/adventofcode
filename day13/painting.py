#! /usr/bin/env python3

class paintPad:
    def __init__(self):
        self.move_arr = []
        self.move_set = set()
        self.painted_tile = dict()
        self.move_arr.append((0,0))
        #0 is N going clockwise
        self.orientation = 0

    def append(self, item):
        #0 is Black
        # row, column ordering
        self.move_arr.append(item)
        self.move_set.add(item)

    def getPaintColor(self, loc):
        if loc in self.painted_tile.keys():
            return self.painted_tile[loc]
        else:
            self.painted_tile[loc] = 0
            return 0

    def drawMap(self, paint, score = 0):
        for x in [' ', '|', '=', '-', 'o']:
            self.painted_tile[x] = 0
        
        for i in range(2, len(paint), 3):
            if paint[i - 2] != -1:
                t = paint[i]
                v = ' '
                if t == 1:
                    v = '|'
                elif t == 2:
                    v = '='
                elif t == 3:
                    v = '-'
                elif t == 4:
                    v = 'o'
                self.painted_tile[v] += 1
                paint[i] = v
            else:

                score = paint[i]
        return paint, score

    def move(self, move, location):
        # 0 is left 1 is right
        if move == 0:
            self.orientation = (4 + self.orientation - 1) % 4
        elif move == 1:
            self.orientation = (4 + self.orientation + 1) % 4
        else:
            raise ValueError('what is happening')

        o = self.orientation
        m = ''
        if o == 0:
            location = (location[0] - 1, location[1])
            m = 'go up'
        elif o == 1:
            location = (location[0], location[1] + 1)
            m = 'go right'
        elif o == 2:
            location = (location[0] + 1, location[1])
            m = 'go down'
        elif o == 3:
            location = (location[0], location[1] - 1)
            m = 'go left'
        else:
            raise ValueError('what is happening')

        return location, m


