#! /usr/bin/env python3

import itertools
from enum import IntEnum
import time

class opCodes(IntEnum):
    add = 1
    multiply = 2
    input = 3
    output = 4
    jit = 5
    jif = 6
    less = 7
    eq = 8
    adj = 9
    exit = 99

    @classmethod
    def has_value(cls, val):
        return val in cls._value2member_map_

    @classmethod
    def is_math(cls, val):
        return val == cls.add or val == cls.multiply

class modeCodes(IntEnum):
    position = 0
    immediate = 1
    relative = 2

    @classmethod
    def is_immediate(cls, v):
        return v == cls.immediate
    @classmethod
    def is_relative(cls, v):
        return v == cls.relative

class IntCode:
    def __init__(self, file, userArr = [], userMode = True):
        with open(file, 'r') as f:
            self.items = f.read().split(",")
        self.items = [int(item) for item in self.items]
#        self.items.extend([0 for x in range(100)])
        self.oldItems = self.items.copy()
        self.inputArr = userArr
        self.userMode = userMode
        self.finalVal = None
        self.progCounter = 0
        self.relativeBase = 0

    def reset(self):
        self.items = self.oldItems.copy()
        self.progCounter = 0
        self.relativeBase = 0
        self.finalVal = None

    def addToInput(self, item):
        self.inputArr.extend(item)

    def runProgWithInputsNoState(self, inputArr):
        self.inputArr = inputArr
        self.oldItems = self.items.copy()
        val = self.runProg()
        self.items = self.oldItems.copy()
        return val

    def runProgWithInputsWithState(self, inputArr, userMode = False):
        self.inputArr = inputArr
        self.userMode = userMode
        val, cont = self.runProg()
        return val, cont

    def runProgWithCurses(self, inputArr = [], userMode = False):
        self.userMode = userMode
        self.inputArr.extend(inputArr)
        # has GUI
        val, cont, m = self.runProg(True)
        return val, cont, m

    def modeR(self, mode, num):
        if modeCodes.is_immediate(mode):
            return int(num)
        if len(self.items) < num + self.relativeBase and self.relativeBase + num > 0:
            rel = self.relativeBase
            arr = [0 for x in range(num + rel + 1 - len(self.items))]
            self.items.extend(arr)
        if modeCodes.is_relative(mode):
            return int(self.items[num + self.relativeBase])
        else:
            return int(self.items[num])

    def interpretInstruction(self, inst):
        op = int(inst[-2:])
        arr = []
        if len(inst) > 2:
            for i in range(len(inst) - 3, -1, -1):
                arr.append(modeCodes(int(inst[i])))
        C = arr[0] if arr and len(arr) > 0 else modeCodes.position
        B = arr[1] if arr and len(arr) > 1 else modeCodes.position
        A = arr[2] if arr and len(arr) > 2 else modeCodes.position
        return op, C, B, A

    def passFail(self, val, i):
        self.finalVal = val
        if val == 0 and self.userMode:
            return 'Pass --> {}'.format(val)
        elif val != 0 and self.items[i] != opCodes.exit and self.userMode:
            return 'Fail --> {}'.format(val)
        elif self.items[i] == opCodes.exit:
            return 'Done --> output: {}'.format(val)
        else:
            return val 

    def jit(self, v1, v2, C, B, i):
        if self.modeR(C, v1) != 0:
            return self.modeR(B, v2)
        else:
            return i + 3

    def jif(self, v1, v2, C, B, i):
        if self.modeR(C, v1) == 0:
            return self.modeR(B, v2)
        else:
            return i + 3

    def less(self, v1, v2, C, B):
        if self.modeR(C, v1) < self.modeR(B, v2):
            return 1
        else:
            return 0

    def eq(self, v1, v2, C, B):
        if self.modeR(C, v1) == self.modeR(B, v2):
            return 1
        else:
            return 0

    def assign(self, A, val, i):
        rel = 0
        if modeCodes.is_relative(A):
            rel = self.relativeBase

        if self.items[i] + rel >= len(self.items):
            arr = [0 for x in range(self.items[i] + rel + 2 - len(self.items))]
            self.items.extend(arr)
#        print(i, rel, len(self.items), self.items[i])
        self.items[self.items[i] + rel] = val

    def adj(self, C, v1):
        self.relativeBase += self.modeR(C, v1)

    def runProg(self, hasGUI = False):
        i = self.progCounter
        while i < len(self.items):
            instruction = str(self.items[i])
            op, C, B, A = self.interpretInstruction(instruction)

            if op == opCodes.add:
                temp = self.modeR(C, self.items[i + 1]) 
                temp += self.modeR(B, self.items[i + 2])
                self.assign(A, temp, i + 3)
                i += 4
            elif op == opCodes.multiply:
                temp = self.modeR(C, self.items[i + 1]) * self.modeR(B, self.items[i + 2])
                self.assign(A, temp, i + 3)
                i += 4
            elif op == opCodes.input:
                if len(self.inputArr) > 0:
                    temp = self.inputArr.pop(0)
                elif self.userMode and not hasGUI:
                    temp = input('Supply parameter input for {}\n'.format(self.items[i:i+2]))
                elif hasGUI and self.userMode:
                    temp = 'Supply Input(Ctrl-G): '
                    return temp, True, 'input'
                elif hasGUI and not self.userMode:
                    temp = self.finalVal
                    return temp, False, 'input'
                else:
                    return self.finalVal, True
                self.assign(C, temp, i + 1)
                i += 2
            elif op == opCodes.output:
                temp = self.modeR(C, self.items[i + 1])
                v = self.passFail(temp, i + 2)
                if self.userMode and not hasGUI and v:
                    print(v)
                elif hasGUI:
                    i += 2
                    self.progCounter = i
                    return v, True, 'output'
                i += 2
            elif op == opCodes.jit:
                temp = self.jit(self.items[i + 1], self.items[i + 2], C, B, i)
                i = temp
            elif op == opCodes.jif:
                temp = self.jif(self.items[i + 1], self.items[i + 2], C, B, i)
                i = temp
            elif op == opCodes.less:
                temp = self.less(self.items[i + 1], self.items[i + 2], C, B)
                self.assign(A, temp, i + 3)
                i += 4
            elif op == opCodes.eq:
                temp = self.eq(self.items[i + 1], self.items[i + 2], C, B)
                self.assign(A, temp, i + 3)
                i += 4
            elif op == opCodes.adj:
                self.adj(C, self.items[i + 1])
                #print(self.items)
                i += 2
            elif op == opCodes.exit:
                if(self.userMode and not hasGUI):
#                    print(self.items)
                    pass
                return 'Exex fin: ' + str(self.finalVal), False, 'end'
            else:
                raise ValueError('incorrect opcode: {}'.format(self.items[i]))
            self.progCounter = i
            if not hasGUI:
                print(opCodes(op), self.progCounter)

            if hasGUI:
                return opCodes(op), True, self.progCounter
#            print(self.items[i - step:i])
