#! /usr/bin/env python3
from IntCodeOptions import IntcodeOptions
import itertools
import curses
from curses.textpad import Textbox, rectangle

class main:
    def __init__(self):
        self.icHandler = IntcodeOptions('input')
        self.menu = [
        'Show Program',
        'Run User Mode - Single',
        'Run Max - Single Pass',
        'Run Max - Feedback',
        'Reset',
        'Exit',
        ]
        self.old_arr = []
        self.count = 0
        self.outputArr = []
        curses.wrapper(self.startCurses)
#        print(self.icHandler.intCode.items)
#        print(self.icHandler.arr2D)

    def startCurses(self, stdscr):
        curses.noecho()
        # Clear and refresh the screen for a blank canvas
        stdscr.clear()
        stdscr.refresh()

        # Start colors in curses
        curses.start_color()
        curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
        curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)
        curses.init_pair(4, curses.COLOR_BLACK, curses.COLOR_CYAN)
        curses.init_pair(5, curses.COLOR_WHITE, curses.COLOR_BLACK)
        curses.init_pair(6, curses.COLOR_BLACK, curses.COLOR_GREEN)

        self.run(stdscr)

    def updateMenu(self, cursor_y, menuscr, exec, runOpt):
        # Declaration of strings
        statusbarstr = "Press 'q' to exit | {} | {}".format(exec, runOpt)

        # Render status bar
        m_h, m_w = menuscr.getmaxyx()
        menuscr.attron(curses.color_pair(3))
        menuscr.addstr(m_h-2, 1, statusbarstr)
        menuscr.addstr(m_h-2, len(statusbarstr) + 1, " " * (m_w - len(statusbarstr) - 2))
        menuscr.attroff(curses.color_pair(3))

        # Render menu text
        menuscr.attron(curses.color_pair(1))
        i = 0
        for m in self.menu:
            if(cursor_y == i):
                menuscr.attroff(curses.color_pair(1))
                menuscr.attron(curses.color_pair(4))
            menuscr.addstr(2 + i, 3, m)
            menuscr.addstr(2 + i, len(m) + 3, " " * (m_w - len(m) - 7))
            if(cursor_y == i):
                menuscr.attroff(curses.color_pair(4))
                menuscr.attron(curses.color_pair(1))
            i += 1
        menuscr.attroff(curses.color_pair(1))

        return cursor_y

    def drawLog(self, scr):
        scr.clear()
        scr.box()
        m_h, m_w = scr.getmaxyx()

        # Render menu text
        scr.attron(curses.color_pair(1))
        i = 0
        for m in self.outputArr:
            scr.addstr(2 + i, 2, str(m))
            i += 1
        scr.attroff(curses.color_pair(1))
        scr.refresh()

    def drawNewProg(self, printscr, h, w, arr):
        self.count = 0
        for y in range(len(arr)):
            for x in range(len(arr[y])):
                rx = self.icHandler.maxItemWidth
                a = arr[y, x]
                printscr.addstr(y , x * rx + 1, '{:^{}}'.format(a, rx))
                self.count += 1

            printscr.refresh()
        self.old_arr = arr.copy()

    def drawPrint(self, printscr, arr, j):
        new_count = 0
        for y in range(len(arr)):
            for x in range(len(arr[y])):
                new_count += 1

        for y in range(len(arr)):
            for x in range(len(arr[y])):
                rx = self.icHandler.maxItemWidth
                a = arr[y][x]
                if self.count != new_count:
                    printscr.addstr(y , x * rx + 1, '{:^{}}'.format(a, rx))
                elif self.old_arr[y][x] != a:
                    printscr.addstr(y , x * rx + 1, '{:^{}}'.format(a, rx))
                    printscr.chgat(y, x * rx + 1, rx, curses.color_pair(6))
                else:
                    printscr.addstr(y , x * rx + 1, '{:^{}}'.format(a, rx))
                printscr.refresh()
                curses.napms(0)

        self.count = new_count
        self.old_arr = arr.copy()

    def runProg(self, i, printscr, messagescr):
        k = None
        h, w = printscr.getmaxyx()
        mh, mw = messagescr.getmaxyx()
        self.icHandler.setSnapshot(w, h, 0)
        arr = self.icHandler.arr2D
        ih, ix = self.icHandler.height, self.icHandler.width
        if i == 0:
            printscr.clear()
            printscr.attron(curses.color_pair(1))
            self.drawNewProg(printscr, h, w, arr)
            printscr.attroff(curses.color_pair(1))
            printscr.refresh()
        elif i == 1:
            cont = True
            j = 0
            inputArray = []
            printscr.attron(curses.color_pair(1))
            while cont:
                messagescr.clear()
                messagescr.box()
                self.drawPrint(printscr, arr, j)
                val, cont, arr = self.icHandler.runSingle(True, inputArray)
                if arr == 'input':
                    inputArray = []
                    messagescr.attron(curses.color_pair(3))
                    messagescr.addstr(mh - 2, 1, val)
                    inputscr = messagescr.subwin(1, 10, h + mh - 1, len(val) + 3)
                    inputscr.attron(curses.color_pair(3))
                    messagescr.refresh()
                    box = Textbox(inputscr)
                    box.edit()
                    m = box.gather()
                    messagescr.attroff(curses.color_pair(3))
                    arr = None
                    inputArray = [int(m)]
                    messagescr.clear()
                    messagescr.refresh()
                elif arr == 'output':
                    messagescr.attron(curses.color_pair(3))
                    messagescr.addstr(mh - 2, 1, str(val))
                    messagescr.refresh()
                    messagescr.attroff(curses.color_pair(3))
                    if len(self.outputArr) > mh - 4:
                        self.outputArr.pop(0) 
                    self.outputArr.append(val)
                    arr = None
                    inputArray = []
                elif arr == 'end':
                    j = 0
                    messagescr.attron(curses.color_pair(3))
                    messagescr.addstr(mh - 2, 1, str(val))
                    messagescr.refresh()
                    messagescr.attroff(curses.color_pair(3))
                    if len(self.outputArr) > mh - 4:
                        self.outputArr.pop(0) 
                    self.outputArr.append(val)
                    inputArray = []
                elif arr != []:
                    j = arr
                    messagescr.attron(curses.color_pair(3))
                    messagescr.addstr(mh - 2, 1 , str(val))
                    messagescr.refresh()
                    messagescr.attroff(curses.color_pair(3))
                    inputArray = []
                self.icHandler.setSnapshot(w, h, j)
                arr = self.icHandler.arr2D
                self.drawPrint(printscr, arr, j)
            self.drawLog(messagescr)
            printscr.attroff(curses.color_pair(1))

        elif i == 2:
            printscr.clear()
            k = ord('q')
        elif i == 4:
            self.icHandler.reset()
        return k

    def run(self, stdscr):
        k = 0
        exec = 'IDLE'
        runOpt = ''
        cursor_y = 0

        height, width = stdscr.getmaxyx()

        # Create menu and output windows
        outscr = curses.newwin(int(height/8 * 3), width, 0, 0)
        outscr.box()
        menuscr = curses.newwin(int(height/4), width, int(height/4 * 3 ), 0)
        messagescr = curses.newwin(int(5 * height/8 - height/4 + 1), width, int(height/8 * 3), 0)
        messagescr.box()
        ho, wo = outscr.getmaxyx()
        printscr = curses.newwin(ho - 2, wo - 2, 1 ,1)

        stdscr.clear()
        stdscr.refresh()
        outscr.refresh()
        messagescr.refresh()

        # Loop where k is the last character pressed
        while (k != ord('q')):
            # Initialization
            curses.curs_set(0)
            menuscr.clear()


            # key moves
            if k == curses.KEY_DOWN or k == ord('j'):
                cursor_y = cursor_y + 1
            elif k == curses.KEY_UP or k == ord('k'):
                cursor_y = cursor_y - 1
            elif k == curses.KEY_ENTER or k == 10 or k == 13:
                exec = 'RUNNING'
                cursor_y = self.updateMenu(cursor_y, menuscr, exec, runOpt)
                menuscr.box()
                menuscr.refresh()
                k = self.runProg(cursor_y, printscr, messagescr)
                exec = 'IDLE'

            cursor_y = max(0, cursor_y)
            cursor_y = min(len(self.menu) - 1, cursor_y)

            cursor_y = self.updateMenu(cursor_y, menuscr, exec, runOpt)

            # draw some borders
            menuscr.box()

            # refresh screen
            menuscr.refresh()
            printscr.refresh()

            # Wait for next input
            if k != ord('q'):
                k = stdscr.getch()

main()
