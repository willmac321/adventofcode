#! /usr/bin/env python3
from Intcode import IntCode
import itertools
import curses
import numpy as np

class IntcodeOptions:
    def __init__(self, file):
        self.intCode = IntCode(file)
        self.height = 0
        self.width = 0
        self.arr2D = []
        self.maxItemWidth = 1
        self.current_row = 0
        self.oldIndex = 0

    def reset(self):
        self.intCode.reset()

    def runSingle(self, userMode, inArr = []):
        return self.intCode.runProgWithCurses(inArr, userMode)

    def findMaxCodeOnePass(self, numInputs):
        #all inputs are unique, if program has error, will discard and try new
        # combo

        outDict = dict()
        out = None
        r = numInputs

        for a, b, c, d, e in\
            itertools.product(range(r), range(r), range(r), range(r), range(r)):
            phaseArr = [a, b, c, d, e]
            if len(phaseArr) == len(set(phaseArr)):
                inputs = phaseArr.copy()
                arr = [phaseArr.pop(0), 0]
                c = 0
                while c < r:
                    try:
                        out = self.intCode.runProgWithInputsNoState(arr)
                        print(out)
                        arr = [phaseArr.pop(0), out]
                    except:
                        break
                if out:
                    outDict[out] = inputs
                c += 1

        print({k: outDict[k] for k in sorted(outDict)})

    def findMaxCodeFeedback(self, lowerB, upperB):
        #all inputs are unique, if program has error, will discard and try new
        # combo

        outDict = dict()
        out = None
        r = upperB - lowerB + 1
        try:
            for a, b, c, d, e in\
                itertools.product(range(r), range(r), range(r), range(r), range(r)):
                phaseArr = [a, b, c, d, e]
                phaseArr = [x + r for x in phaseArr]
                if len(phaseArr) == len(set(phaseArr)):
                    progArr = [[True, IntCode('input')] for x in range(r)]
                    inputs = phaseArr.copy()
                    arr = [phaseArr.pop(0), 0]
                    c = 0
                    cont = True
                    while progArr[c % r][0]:
                    #    try:
                        out, progArr[c % r][0] = progArr[c % r][1].runProgWithInputsWithState(arr)
                        if c < r - 1:
                            arr = [phaseArr.pop(0), out]
                        else:
                            arr = [out]
#                        except:
#                            break
                        c += 1

                    if out and not progArr[r - 1][0] :
                        outDict[out] = inputs

            print({k: outDict[k] for k in sorted(outDict)})
        except KeyboardInterrupt:
            sys.exit()

    def test(self):
        self.intCode = IntCode('testinput')
        outDict = dict()
        out = None
        phaseArr = [4, 3, 2, 1, 0]
        r = len(phaseArr)
        if len(phaseArr) == len(set(phaseArr)):
            inputs = phaseArr.copy()
            arr = [phaseArr.pop(0), 0]
            c = 0
            while c < r:
                out = self.intCode.runProgWithInputs(arr)
                print(phaseArr)
                print(out)
                if phaseArr:
                    arr = [phaseArr.pop(0), out]
                c += 1
            if out:
                outDict[out] = inputs

        print({k: outDict[k] for k in sorted(outDict)})

    def getItems(self):
        return self.intCode.items

    def setSnapshot(self, x, y, startIndex = 0):
        #find max item width and use that
        self.maxItemWidth = len(str(self.intCode.items[0])) + 2
        for i in self.intCode.items:
            if len(str(i)) + 2 > self.maxItemWidth:
                self.maxItemWidth = len(str(i)) + 2

        self.width = x // self.maxItemWidth
        rows = y 
        if rows > len(self.intCode.items) // self.width:
            startIndex = 0
        if divmod(startIndex, self.width)[0] == self.current_row:
            startIndex = self.oldIndex
        else:
            self.current_row = startIndex // self.width
            self.oldIndex = startIndex
        if rows > len(self.intCode.items[startIndex:]) // self.width:
            self.height = len(self.intCode.items[startIndex:]) // self.width + 1
        else:
            self.height = rows
        self.arr2D = np.zeros([self.height, self.width], dtype=np.int_)
        i = startIndex
        for row in range(self.height):
            for col in range(self.width):
                if i < len(self.intCode.items[startIndex:]):
                    self.arr2D[row, col] = self.intCode.items[i]
                    i += 1
        

