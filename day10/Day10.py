#! /usr/bin/env python3

import sys
import numpy as np
import math
import operator
from fractions import Fraction

class AsteroidField:
    def __init__(self, file):
        with open(file, 'r') as f:
            arr = f.read().split()
            self.ast_arr = np.array([[b for b in a] for a in arr])
        self.meteor = []

        for row in range(len(self.ast_arr)):
            for col in range(len(self.ast_arr[row])):
                if self.ast_arr[row][col] == '#':
                    self.meteor.append([(row, col), None, None])
        self.meteor_orig = self.meteor.copy()
        self.max = None
        self.spotted_arr = []

    def find200(self):
        copy = self.meteor_orig.copy()
        index = 0
        self.findMax()
#        self.knownO(self.max, copy)
#
#        self.spotted_arr = sorted(self.spotted_arr, key=lambda x: (180 - math.degrees(math.atan2(*tuple(map(operator.sub, x[0], self.max))[::-1]))) % 360)
#        print()
#        self.drawSingle(self.max, self.spotted_arr)
#        print(self.spotted_arr)
        a = 0
        while a < 200 or a <= len(copy) - 1:
            print()
            self.knownO(self.max, copy)
            self.spotted_arr = sorted(self.spotted_arr, key=lambda x: (180 - math.degrees(math.atan2(*tuple(map(operator.sub, x[0], self.max))[::-1]))) % 360)
            for r in self.spotted_arr:
                index = None
                for i, b in enumerate(copy):
                    if b[0] == (r[0][0],r[0][1]):
                        index = i
                a += 1
                if a == 200:
                    print(copy.pop(index))
                    break
                copy.pop(index)
            print(a)
                        
        self.knownO(self.max, copy)
        self.spotted_arr = sorted(self.spotted_arr, key=lambda x: (180 - math.degrees(math.atan2(*tuple(map(operator.sub, x[0], self.max))[::-1]))) % 360)
# put in order based on dist, starting from slope 1,0
# pop from array
# repeat with new copy calcing and sorting until 200


    def test(self, origin):
        self.meteor = self.meteor_orig.copy()
        self.addSlopesFromPoint(origin[0], origin[1])
        print(self.meteor)
        self.dropSameSlopesFromMeteor()
        print(self.spotted_arr)
        self.drawSingle(origin, self.spotted_arr)

    def knownO(self, origin, copy):
        self.meteor = copy.copy()
        self.addSlopesFromPoint(origin[0], origin[1])
        self.dropSameSlopesFromMeteor()
        #self.drawSingle(origin, self.spotted_arr)

    def drawSingle(self, orig, spotted_arr):
        arr = np.array([['.' for b in range(len(self.ast_arr[0]))] for a in range(len(self.ast_arr))])
        count = 0
        for i in spotted_arr:
            arr[i[0][0], i[0][1]] = '#'

        arr[orig[0], orig[1]] = '0'
        print(arr)

    def findMax(self):
        maxPoint = (0,0)
        maxVal = 0
        maxMeteor = []

        for m in self.meteor_orig:
            self.meteor = self.meteor_orig.copy()
            self.addSlopesFromPoint(m[0][0], m[0][1])
            self.dropSameSlopesFromMeteor()
            l = len(self.spotted_arr)
            maxMeteor.append([m[0], l])
            if l > maxVal:
                maxVal = l
                maxPoint = m[0]
        self.max = maxPoint
        self.draw(maxPoint, maxVal, maxMeteor)

    def addSlopesFromPoint(self, row, col):
        # slopek is tuple of rise, run
        for k, m in enumerate(self.meteor):
            self.meteor[k] = [m[0], self.simplify(row - m[0][0], col - m[0][1]), abs(col - m[0][1]) + abs(row - m[0][0])] 
        p = None
        for k, m in enumerate(self.meteor):
            if m[1][0] == 0 and m[1][1] == 0:
                p = k
        self.meteor.pop(p)

    def dropSameSlopesFromMeteor(self):
        new_dict = {}
        for k, m in enumerate(self.meteor):
            for l, n in enumerate(self.meteor):
                if k == l:
                    pass
                elif m[1] == n[1]: 
                    #take one with smallest dist
                    if m[2] < n[2]:
                        new_dict[m[1]] = (m)
                    elif m[2] > n[2]:
                        new_dict[n[1]] = (n)
                elif not n[1] in new_dict:
                    new_dict[n[1]] = n
                elif new_dict[n[1]][2] > n[2]:
                    new_dict[n[1]] = n
        self.spotted_arr = list(new_dict.values())

    def simplify(self, rise, run):
        if run == 0 and rise == 0:
            return (rise, run)
        elif run == 0:
            return (rise/abs(rise), run)
        elif rise == 0:
            return (rise, run/abs(run))
        else:
            sign_rise = rise/abs(rise)
            sign_run = run/abs(run)
            f = Fraction(rise, run)
            return (int(abs(f.numerator) * sign_rise), int(abs(f.denominator) * sign_run))

    def draw(self, orig, l, spotted_arr):
        print(spotted_arr)
        arr = np.array([['{:^{}}'.format('.',3) for b in range(len(self.ast_arr[0]))] for a in range(len(self.ast_arr))])
        count = 0
        for i in spotted_arr:
            arr[i[0][0], i[0][1]] = i[1]
        print(arr)
        print((orig[1], orig[0]))
        print(l)
        
ast = AsteroidField('input')
#ast.test((2,0))
#ast.findMax()
ast.find200()
#row col (rise, run)
#ast.draw(origin, ast.spotted_arr)
