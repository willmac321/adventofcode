#! /usr/bin/env python3

import math

# store coords of each turn in array

class day3:
    def __init__(self):
        line1_arr = []
        line2_arr = []
        intersections = []

    def calcP1(self, arr1, arr2, origin):
        points1 = self.createNodes(arr1, origin)
        points2 = self.createNodes(arr2, origin)
        print(points1)
        print(points2)
        intersects = self.findIntersectionSegment(points1, points2, origin)
        print(intersects)
        point = self.findClosestByCityBlock(intersects, origin, float("inf"), origin)
        return point

    def calcP2(self, arr1, arr2, origin):
        #find by least steps - in theory, should be first matching intersect point between two intersection sets
        points1 = self.createNodes(arr1, origin)
        points2 = self.createNodes(arr2, origin)
        intersects_points1First = self.findIntersectionSegment(points1, points2, origin)
        intersects_points2First = self.findIntersectionSegment(points2, points1, origin)
        print(intersects_points1First)
        print(intersects_points2First)
        arrIntsct = self.findClosestBySteps(intersects_points1First, intersects_points2First)
        index = self.findMin2dPoint(arrIntsct, (float("inf"), float("inf")))
        closestPoint = intersects_points1First[index[0]]
        steps = self.countSteps(points1, closestPoint)
        steps += self.countSteps(points2, closestPoint)
        return steps

    def countSteps(self, points, closest):

        cont = True
        c = 0
        dist = 0

        while(cont):
            
            seg1P1, seg1P2 = self.sortTwoPoints(points[c], points[c + 1], (0,0))
            seg2P1, seg2P2 = closest, closest
            if seg1P1[0] >= seg2P1[0] and seg1P1[0] <= seg2P2[0]\
                    and seg2P1[1] >= seg1P1[1] and seg2P1[1] <= seg1P2[1]:
                        cont = False
            elif seg1P1[1] >= seg2P1[1] and seg1P1[1] <= seg2P2[1]\
                    and seg2P1[0] >= seg1P1[0] and seg2P1[0] <= seg1P2[0]:
                        cont = False
            ##CHECK Q2
            elif seg1P1[0] >= seg2P1[0] and seg1P1[0] <= seg2P2[0]\
                    and seg2P1[1] <= seg1P1[1] and seg2P1[1] >= seg1P2[1]:
                        cont = False
            elif seg1P1[1] >= seg2P1[1] and seg1P1[1] <= seg2P2[1]\
                    and seg2P1[0] <= seg1P1[0] and seg2P1[0] >= seg1P2[0]:
                        cont = False
            ##CHECK Q3
            elif seg1P1[0] <= seg2P1[0] and seg1P1[0] >= seg2P2[0]\
                    and seg2P1[1] <= seg1P1[1] and seg2P1[1] >= seg1P2[1]:
                        cont = False
            elif seg1P1[1] <= seg2P1[1] and seg1P1[1] >= seg2P2[1]\
                    and seg2P1[0] <= seg1P1[0] and seg2P1[0] >= seg1P2[0]:
                        cont = False
            ##CHECK Q4
            elif seg1P1[0] <= seg2P1[0] and seg1P1[0] >= seg2P2[0]\
                    and seg2P1[1] >= seg1P1[1] and seg2P1[1] <= seg1P2[1]:
                        cont = False
            elif seg1P1[1] <= seg2P1[1] and seg1P1[1] >= seg2P2[1]\
                    and seg2P1[0] >= seg1P1[0] and seg2P1[0] <= seg1P2[0]:
                        cont = False

            if cont:
                dist += abs(points[c][0] - points[c + 1][0]) + abs(points[c][1] - points[c + 1][1])
            else:
                dist += abs(points[c][0] - closest[0]) + abs(points[c][1] - closest[1])


            c += 1
            if(c >= len(points)):
                cont = False

        return dist



    def findMin2dPoint(self, arr, closest):
        p1 = arr.pop(0)
        closest, p1 = self.sortTwoPoints(p1, closest, (0, 0))
        if len(arr) == 0:
            return closest
        else:
            return self.findMin2dPoint(arr, closest)


    def findClosestBySteps(self, i1, i2):
        arr = []
        for k, item1 in enumerate(i1):
            t = (k, 0)
            for j, item2 in enumerate(i2):
                if  item1[0] == item2[0] and item1[1] == item2[1]:
                    t = (t[0], j)
                    arr.append(t)
        return arr

    def findClosestByCityBlock(self, arr, origin, last, point):
        #exclude origin
        if len(arr) == 0:
            return point, last

        a = arr.pop(0)
        d = abs(origin[0] - a[0]) + abs(origin[1] - a[1])

        if d == 0:
            return self.findClosestByCityBlock(arr, origin, last, point)
        elif d < last:
            return self.findClosestByCityBlock(arr, origin, d, a)
        else:
            return self.findClosestByCityBlock(arr, origin, last, point)



    def findIntersectionSegment(self, p1, p2, o):
        # if segment 1 and segment 2 are perpindicular keep going else next seg
        # if segment 1 x is inside segment 2 x then intersect
        # if segment 1 y is inside segment 2 y then intersect

        arr = []

        for i in range(1, len(p1) - 1):
            seg1P1, seg1P2 = self.sortTwoPoints(p1[i], p1[i + 1], o)
            for j in range(1, len(p2) - 1):
                seg2P1, seg2P2 = self.sortTwoPoints(p2[j], p2[j + 1], o)
                ##Check Q1
                if seg1P1[0] >= seg2P1[0] and seg1P1[0] <= seg2P2[0]\
                        and seg2P1[1] >= seg1P1[1] and seg2P1[1] <= seg1P2[1]:
                    print('seg1: {},{}, seg2: {},{} intersect:{}'.format(seg1P1,seg1P2,seg2P1,seg2P2,(seg1P1[0], seg2P1[1])))
                    arr.append((seg1P1[0], seg2P1[1]))
                elif seg1P1[1] >= seg2P1[1] and seg1P1[1] <= seg2P2[1]\
                        and seg2P1[0] >= seg1P1[0] and seg2P1[0] <= seg1P2[0]:
                    print('seg1: {},{}, seg2: {},{} intersect:{}'.format(seg1P1,seg1P2,seg2P1,seg2P2,(seg2P1[0], seg1P1[1])))
                    arr.append((seg2P1[0], seg1P1[1]))
                ##CHECK Q2
                elif seg1P1[0] >= seg2P1[0] and seg1P1[0] <= seg2P2[0]\
                        and seg2P1[1] <= seg1P1[1] and seg2P1[1] >= seg1P2[1]:
                    print('seg1: {},{}, seg2: {},{} intersect:{}'.format(seg1P1,seg1P2,seg2P1,seg2P2,(seg1P1[0], seg2P1[1])))
                    arr.append((seg1P1[0], seg2P1[1]))
                elif seg1P1[1] >= seg2P1[1] and seg1P1[1] <= seg2P2[1]\
                        and seg2P1[0] <= seg1P1[0] and seg2P1[0] >= seg1P2[0]:
                    print('seg1: {},{}, seg2: {},{} intersect:{}'.format(seg1P1,seg1P2,seg2P1,seg2P2,(seg2P1[0], seg1P1[1])))
                    arr.append((seg2P1[0], seg1P1[1]))
                ##CHECK Q3
                elif seg1P1[0] <= seg2P1[0] and seg1P1[0] >= seg2P2[0]\
                        and seg2P1[1] <= seg1P1[1] and seg2P1[1] >= seg1P2[1]:
                    print('seg1: {},{}, seg2: {},{} intersect:{}'.format(seg1P1,seg1P2,seg2P1,seg2P2,(seg1P1[0], seg2P1[1])))
                    arr.append((seg1P1[0], seg2P1[1]))
                elif seg1P1[1] <= seg2P1[1] and seg1P1[1] >= seg2P2[1]\
                        and seg2P1[0] <= seg1P1[0] and seg2P1[0] >= seg1P2[0]:
                    print('seg1: {},{}, seg2: {},{} intersect:{}'.format(seg1P1,seg1P2,seg2P1,seg2P2,(seg2P1[0], seg1P1[1])))
                    arr.append((seg2P1[0], seg1P1[1]))
                ##CHECK Q4
                elif seg1P1[0] <= seg2P1[0] and seg1P1[0] >= seg2P2[0]\
                        and seg2P1[1] >= seg1P1[1] and seg2P1[1] <= seg1P2[1]:
                    print('seg1: {},{}, seg2: {},{} intersect:{}'.format(seg1P1,seg1P2,seg2P1,seg2P2,(seg1P1[0], seg2P1[1])))
                    arr.append((seg1P1[0], seg2P1[1]))
                elif seg1P1[1] <= seg2P1[1] and seg1P1[1] >= seg2P2[1]\
                        and seg2P1[0] >= seg1P1[0] and seg2P1[0] <= seg1P2[0]:
                    print('seg1: {},{}, seg2: {},{} intersect:{}'.format(seg1P1,seg1P2,seg2P1,seg2P2,(seg2P1[0], seg1P1[1])))
                    arr.append((seg2P1[0], seg1P1[1]))
        return arr


    def sortTwoPoints(self, p1, p2, o):
        dist1 = math.sqrt(math.pow(p1[0] - o[0], 2) + math.pow(p1[1] - o[1], 2))
        dist2 = math.sqrt(math.pow(p2[0] - o[0], 2) + math.pow(p2[1] - o[1], 2))

        if abs(dist1) > abs(dist2):
            return p2, p1
        else:
            return p1, p2

    def findIntersectionsLine(self, p1, p2):
        arr = []

        for i in range(len(p1) - 1):
            line1 = self.lineEquationFlat(p1[i], p1[i + 1])
            for j in range(len(p2) - 1):
                line2 = self.lineEquation(p2[j], p2[j + 1])
                if (line1[0] - line2[0]) != 0:
                    #check if lines are within each other
                    x = (line2[1] - line1[1]) / (line1[0] - line2[0])
                    y = line1[0] * x + line1[1]
                    arr.append((x, y))

        return arr

    def createNodes(self, arr, point):
#        arr: array with series of moves D1, R13, etc
#        start: tuple with start coords as (x, y) ie (0, 0)
        points = self.makeBigTuple(arr, point)
        return self.remakeTuples(points)
    
    def makeBigTuple(self, arr, point):
        b = arr.pop(0)
        c = point
        
        if b[0] == 'R':
            c = (c[0] + int(b[1:]), c[1])
        elif b[0] == 'D':
            c = (c[0], c[1] - int(b[1:]))
        elif b[0] == 'L':
            c = (c[0] - int(b[1:]), c[1])
        elif b[0] == 'U':
            c = (c[0], c[1] + int(b[1:]))
        else:
            raise ValueError('First item is not direction')

        if len(arr) <= 0:
            return point + c
        else:
            return point + self.makeBigTuple(arr, c)
            
    def remakeTuples(self, p):
        o = []
        for i in range(0, len(p), 2):
            o.append((p[i], p[i + 1]))
        return o


ex1 = 'R8,U5,L5,D3'#'R75,D30,R83,U83,L12,D49,R71,U7,L72'
ex2 = 'U7,R6,D4,L4'#'U62,R66,U55,R34,D71,R55,D58,R83'
with open('day3input', 'r') as f:
    item1 = f.readline()
    item2 = f.readline()

print(day3().calcP2(item1.split(','), item2.split(','), (0,0)))
