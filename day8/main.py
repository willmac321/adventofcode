import curses
from curses.textpad import rectangle
import sys, os
from SIF import SIF

class main:
    def __init__(self):
        self.sif = SIF(25, 6)
        self.sif.setImagedata('input')
        self.menu = [
        'Combine Layers',
        'Find Min 0 Layer',
        'Exit',
        ]
        curses.wrapper(self.startCurses)


    def startCurses(self, stdscr):
        # Clear and refresh the screen for a blank canvas
        stdscr.clear()
        stdscr.refresh()

        # Start colors in curses
        curses.start_color()
        curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
        curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)
        curses.init_pair(4, curses.COLOR_BLACK, curses.COLOR_CYAN)
        curses.init_pair(5, curses.COLOR_WHITE, curses.COLOR_BLACK)
        curses.init_pair(6, curses.COLOR_BLACK, curses.COLOR_GREEN)

        self.run(stdscr)

    def updateMenu(self, cursor_y, menuscr, exec, runOpt):
        # Declaration of strings
        statusbarstr = "Press 'q' to exit | {} | {}".format(exec, runOpt)

        # Render status bar
        m_h, m_w = menuscr.getmaxyx()
        menuscr.attron(curses.color_pair(3))
        menuscr.addstr(m_h-2, 1, statusbarstr)
        menuscr.addstr(m_h-2, len(statusbarstr) + 1, " " * (m_w - len(statusbarstr) - 2))
        menuscr.attroff(curses.color_pair(3))

        # Render menu text
        menuscr.attron(curses.color_pair(1))
        i = 0
        for m in self.menu:
            if(cursor_y == i):
                menuscr.attroff(curses.color_pair(1))
                menuscr.attron(curses.color_pair(4))
            menuscr.addstr(2 + i, 3, m)
            menuscr.addstr(2 + i, len(m) + 3, " " * (m_w - len(m) - 7))
            if(cursor_y == i):
                menuscr.attroff(curses.color_pair(4))
                menuscr.attron(curses.color_pair(1))
            i += 1
        menuscr.attroff(curses.color_pair(1))

        return cursor_y

    def drawPrint(self, printscr, sx, sy, arr):
        for y in range(len(arr)):
            for x in range(len(arr[y])):
                a = int(arr[y][x])
                #printscr.addstr(y , x, str(a)) # , '{:^{}}'.format(a, rx))
                rx = 1
                if a == 0:
                    printscr.chgat(y + sy , x + sx , rx, curses.color_pair(6) )
                elif a == 1:
                    printscr.chgat(y + sy , x + sx , rx, curses.color_pair(3) )
                elif a == 2:
                    printscr.chgat(y + sy , x + sx , rx, curses.color_pair(5) )
                printscr.refresh()

            curses.napms(1)

    def runProg(self, i, printscr):

        k = None
        h, w = printscr.getmaxyx()
        ih, ix = self.sif.height, self.sif.width
        printscr.clear()
        if i == 0:
# get x and y of array and of window to stretch to full
# 3 5 6
            printscr.attron(curses.color_pair(2))
            for j in range(len(self.sif.layers)):
                arr = self.sif.combineLayers(True, j)
                starty, startx = h // 2 - len(arr) // 2, w // 2 - len(arr[0]) // 2
                self.drawPrint(printscr, startx, starty, arr)
            printscr.attroff(curses.color_pair(2))
        elif i == 1:
            index = self.sif.findMinZeros()
            self.sif.composite = self.sif.layers[index].copy()
            arr = self.sif.create2DArray()
            starty, startx = h // 2 - len(arr) // 2, w // 2 - len(arr[0]) // 2
            self.drawPrint(printscr, startx, starty, arr)

            printscr.addstr(h - 1, 0, 'Product of 1\'s and 2\'s: {}'.format(self.sif.onesAndTwos(index)))

        elif i == 2:
            printscr.clear()
            k = ord('q')
        return k

    def run(self, stdscr):
        k = 0
        exec = 'IDLE'
        runOpt = ''
        cursor_y = 0

        height, width = stdscr.getmaxyx()

        # Create menu and output windows
        outscr = curses.newwin(int(height/4 * 3), width, 0, 0)
        outscr.box()
        menuscr = curses.newwin(int(height/4), width, int(height/4 * 3), 0)
        ho, wo = outscr.getmaxyx()
        printscr = curses.newwin(ho - 2, wo - 2, 1 ,1)

        stdscr.clear()
        stdscr.refresh()
        outscr.refresh()

        # Loop where k is the last character pressed
        while (k != ord('q')):
            # Initialization
            curses.curs_set(0)
            menuscr.clear()


            # key moves
            if k == curses.KEY_DOWN or k == ord('j'):
                cursor_y = cursor_y + 1
            elif k == curses.KEY_UP or k == ord('k'):
                cursor_y = cursor_y - 1
            elif k == curses.KEY_ENTER or k == 10 or k == 13:
                exec = 'RUNNING'
                cursor_y = self.updateMenu(cursor_y, menuscr, exec, runOpt)
                menuscr.box()
                menuscr.refresh()
                k = self.runProg(cursor_y, printscr)
                exec = 'IDLE'

            cursor_y = max(0, cursor_y)
            cursor_y = min(len(self.menu) - 1, cursor_y)

            cursor_y = self.updateMenu(cursor_y, menuscr, exec, runOpt)

            # draw some borders
            menuscr.box()

            # refresh screen
            menuscr.refresh()
            printscr.refresh()

            # Wait for next input
            if k != ord('q'):
                k = stdscr.getch()


main()
