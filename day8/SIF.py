#! /usr/bin/env python3
from enum import Enum
from PIL import Image, ImageDraw
import numpy as np

class Colors(Enum):
    black = 0
    white = 1
    transparent = 2

class SIF:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.layers = []
        self.composite = []
        self.progC = 0

    def setImagedata(self, file):
        with open(file, 'r') as f:
            self.items = f.read().split()
            self.items = [Colors(int(i)) for i in str(self.items[0])]
        self.splitLayers()

    def splitLayers(self):
        layer = 0
        while layer * self.width * self.height < len(self.items):
            lower_b = layer * self.width * self.height
            upper_b = (layer + 1) * self.width * self.height
            self.layers.append(self.items[lower_b:upper_b])
            layer += 1

    def combineLayers(self, outscr = False, i = 0):
        if not outscr:
            self.composite = self.layers[0].copy()
            for layer in self.layers:
                for i in range(len(self.composite)):
                    if self.composite[i] == Colors.transparent:
                        self.composite[i] = layer[i]
#            print([x.value for x in self.composite])
            return
        else:
            if i == 0:
                self.composite = self.layers[i].copy()
            else:
                for j in range(len(self.composite)):
                    if self.composite[j] == Colors.transparent:
                        self.composite[j] = self.layers[i][j]
            return self.create2DArray()


    def create2DArray(self):
        arr = np.ones([self.height, self.width], dtype=np.uint8)
        im_arr = np.ones([self.height, self.width], dtype=np.uint8)
#        print([x.value for x in self.composite])
        count = 0
        for y in range(self.height):
            for x in range(self.width):
                arr[y, x] = 2
                arr[y, x] = self.composite[count].value
                im_arr[y,x] = self.composite[count].value * 255
                count += 1
        img = Image.fromarray(im_arr, mode='L')
        img.save('out.png')

        return arr


    def findMinZeros(self):
        index = None
        count = float("inf")
        for k, layer in enumerate(self.layers):
            c = layer.count(Colors.black)
            if c < count:
                 count = c
                 index = k
        return index

    def onesAndTwos(self, i):
        ones, twos = self.layers[i].count(Colors.white), self.layers[i].count(Colors.transparent)
        return ones * twos

if __name__=="__main__":
    s = SIF(2, 2)
    s.setImagedata('test')
    s.combineLayers()
    print(s.create2DArray())
