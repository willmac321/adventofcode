Made a curses module for this guy
![](curses.gif)

Decode int to find layer with fewest 0 digits
  - answer is all 1's times 2's

  Second part is to decode image,
  layers are black and white with early layers on top of later layers
