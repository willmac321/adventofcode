#! /usr/bin/env python3

import math

# store coords of each turn in array

class day4:
    def __init__(self, item):
        item = item.split("-")
        self.lowerBound = int(item[0])
        self.upperBound = int(item[1])

    def findAllPossiblePasswordsP1(self):
        #rules:
        #   6 digit number
        #   within range given
        #   two adj digits are same
        #   left to right digits never decrease
        arr = []

        for n in range(self.lowerBound, self.upperBound):
            ok = True
            same = False
            for i in range(1, len(str(n))):
                if int(str(n)[i - 1]) > int(str(n)[i]):
                    ok = False
            for i in range(1, len(str(n))):
                if int(str(n)[i - 1]) == int(str(n)[i]):
                    same = True
            if ok and same:
                arr.append(n)
                
        return arr
                 
    def findAllPossiblePasswordsP2(self):
        #rules:
        #   6 digit number
        #   within range given
        #   two adj digits are same
        #   left to right digits never decrease
        arr = []

        for n in range(self.lowerBound, self.upperBound):
            ok = True
            same = False
            for i in range(1, len(str(n))):
                if int(str(n)[i - 1]) > int(str(n)[i]):
                    ok = False
                if str(n).count(str(n)[i]) == 2:
                    same = True

            if ok and same:
                arr.append(n)
                
        return arr
                

with open('day4input', 'r') as f:
    item = f.read()

print(len(day4(item).findAllPossiblePasswordsP2()))
