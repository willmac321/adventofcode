#! /usr/bin/env python3

import itertools
from enum import IntEnum

class opCodes(IntEnum):
    add = 1
    multiply = 2
    input = 3
    output = 4
    jit = 5
    jif = 6
    less = 7
    eq = 8
    exit = 99

    @classmethod
    def has_value(cls, val):
        return val in cls._value2member_map_

    @classmethod
    def is_math(cls, val):
        return val == cls.add or val == cls.multiply

class modeCodes(IntEnum):
    position = 0
    immediate = 1

class day5:
    def __init__(self):
        with open('day5input', 'r') as f:
            self.items = f.read().split(",")
        self.items = [int(item) for item in self.items]

    def modeR(self, mode, num):
        if self.isImmediate(mode):
            return int(num)
        else:
            return int(self.items[num])

    def isImmediate(self, mode):
        if mode == modeCodes.immediate:
            return True
        elif mode == modeCodes.position:
            return False

    def interpretInstruction(self, inst):
        op = int(inst[-2:])
        arr = []
        if len(inst) > 2:
            for i in range(len(inst) - 3, -1, -1):
                arr.append(modeCodes(int(inst[i])))
        C = arr[0] if arr and len(arr) > 0 else modeCodes.position
        B = arr[1] if arr and len(arr) > 1 else modeCodes.position
        A = arr[2] if arr and len(arr) > 2 else modeCodes.position
        return op, C, B, A

    def passFail(self, val, i):
        if val == 0:
            return 'Pass --> {}'.format(val)
        if val != 0 and self.items[i] != opCodes.exit:
            return 'Fail --> {}'.format(val)
        else:
            return 'Done --> output: {}'.format(val)

    def jit(self, v1, v2, C, B, i):
        if self.modeR(C, v1) != 0:
            return self.modeR(B, v2)
        else:
            return i + 3

    def jif(self, v1, v2, C, B, i):
        if self.modeR(C, v1) == 0:
            return self.modeR(B, v2)
        else:
            return i + 3

    def less(self, v1, v2, C, B):
        if self.modeR(C, v1) < self.modeR(B, v2):
            return 1
        else:
            return 0

    def eq(self, v1, v2, C, B):
        if self.modeR(C, v1) == self.modeR(B, v2):
            return 1
        else:
            return 0

    def runProg(self):
        i = 0
        while i < len(self.items):
            instruction = str(self.items[i])
            op, C, B, A = self.interpretInstruction(instruction)

            if op == opCodes.add:
                temp = self.modeR(C, self.items[i + 1]) + self.modeR(B, self.items[i + 2])
                self.items[self.items[i + 3]] = temp
                i += 4
                step = 4
            elif op == opCodes.multiply:
                temp = self.modeR(C, self.items[i + 1]) * self.modeR(B, self.items[i + 2])
                self.items[self.items[i + 3]] = temp
                i += 4
                step = 4
            elif op == opCodes.input:
                temp = input('Supply parameter input for {}\n'.format(self.items[i:i+2]))
                self.items[self.items[i + 1]] = temp
                i += 2
                step = 2
            elif op == opCodes.output:
                temp = self.modeR(C, self.items[i + 1])
                print(self.passFail(temp, i + 2))
                i += 2
                step = 2
            elif op == opCodes.jit:
                temp = self.jit(self.items[i + 1], self.items[i + 2], C, B, i)
                i = temp
            elif op == opCodes.jif:
                temp = self.jif(self.items[i + 1], self.items[i + 2], C, B, i)
                i = temp
            elif op == opCodes.less:
                temp = self.less(self.items[i + 1], self.items[i + 2], C, B)
                self.items[self.items[i + 3]] = temp
                i += 4
            elif op == opCodes.eq:
                temp = self.eq(self.items[i + 1], self.items[i + 2], C, B)
                self.items[self.items[i + 3]] = temp
                i += 4
            elif op == opCodes.exit:
#                self.printOp(i, 0)
                break
            else:
                raise ValueError('incorrect opcode: {}'.format(self.items[i]))
#            print(self.items[i - step:i])


prob2 = day5()
print()
prob2.runProg()
#prob2.solveForVal(19690720)
