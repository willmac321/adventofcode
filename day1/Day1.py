#!/usr/bin/env python3

import sys
import math


# Fuel req to launch module based on mass 
# fuel = rounddown (mass / 3) - 2
# find fuel for each input then add all together

class day1:
    def __init__(self):
        with open('day1input', 'r') as f:
            self.items = list(f)
        self.fuelTotal = 0

    def solveP1(self):
        for item in self.items:
            self.fuelTotal += math.floor(int(item) / 3) - 2
    
    def solveP2(self):
        for item in self.items:
            self.fuelTotal += self.recurse(item)

    def recurse(self, item):
        tempFuel = math.floor(int(item) / 3) - 2
        if tempFuel <= 0:
            return 0
        else:
            return tempFuel + self.recurse(tempFuel)


    
prob1 = day1()
prob1.solveP2()
print(prob1.fuelTotal)
